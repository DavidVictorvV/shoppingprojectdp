function createCard({ name, ingredients, price, ...item }, id, key) {
  let card = document.createElement("div");
  card.className = "card mt-4";
  card.style = "width: 18rem";

  let cardBody = document.createElement("div");
  cardBody.className = "card-body d-flex flex-column";

  let title = document.createElement("h5");
  title.className = "card-title";
  title.innerHTML = name;

  let text = document.createElement("div");
  text.className = "card-text";
  if (ingredients) text.innerHTML = ingredients;

  let priceEl = document.createElement("div");
  priceEl.className = "float-end";
  priceEl.innerHTML = price + "RON" + "(VAT included)";

  let buttonDiv = document.createElement("div");

  let link = document.createElement("a");
  link.className = "btn btn-primary float-end";
  let cart = window.localStorage.getItem("cart");
  link.innerHTML = "Add to cart";
  if (cart.includes(id)) link.innerHTML = "Remove";
  link.onclick = () => {
    if (link.innerHTML == "Add to cart") {
      addItem({ id, name, ingredients, price, ...item }, key);
      link.innerHTML = "Remove";
    } else {
      removeItem(id);
      link.innerHTML = "Add to cart";
    }
  };

  cardBody.appendChild(title);
  cardBody.appendChild(text);
  text.appendChild(priceEl);

  cardBody.appendChild(buttonDiv);
  buttonDiv.appendChild(link);
  card.appendChild(cardBody);
  cardContainer = document.getElementById("cardContainer");
  cardContainer.appendChild(card);
}

axios({
  method: "GET",
  url: "https://designpaterns-db6fc-default-rtdb.europe-west1.firebasedatabase.app/Menu.json",
  dataType: "jsonp",
}).then(function (res) {
  cardContainer = document.getElementById("cardContainer");

  for (const [key, value] of Object.entries(res.data)) {
    let newType = document.createElement("h4");
    newType.innerHTML = key + ":";
    cardContainer.appendChild(newType);
    for (const [key2, value2] of Object.entries(value)) {
      createCard(value2, key2, key);
    }
  }
});

function addItem(id, key) {
  let cart = JSON.parse(window.localStorage.getItem("cart"));
  let newList = cart ? cart : [];
  newList.push(key, id);

  let storage = window.localStorage;
  storage.setItem("cart", JSON.stringify(newList));
}

function removeItem(id) {
  let cart = JSON.parse(window.localStorage.getItem("cart"));
  let newList = cart;
  for (let i = 0; i < newList.length; i++) {
    if (newList[i].id == id) {
      newList.splice(newList[i], 1);
      newList.splice(newList[i - 1], 1);
    }
  }
  let storage = window.localStorage;
  storage.setItem("cart", JSON.stringify(newList));
}
