export default class Sauce {
  constructor(options) {
    this.name = options.name || "new sauce";
    this.price = options.price || 0;
  }

  description = function () {
    console.log(`${this.constructor.name}: ${this.name}`);
  };
}
