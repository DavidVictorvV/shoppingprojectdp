export default class Pizza {
  constructor(options) {
    this.name = options.name || "new pizza";
    this.ingredients = options.ingredients;
    this.price = options.price || 0;
  }

  description = function () {
    console.log(
      `${this.constructor.name} ${this.name}: \n Ingredients: ${this.ingredients}`
    );
  };
}
