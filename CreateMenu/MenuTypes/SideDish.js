export default class SideDish {
  constructor(options) {
    this.name = options.name || "new side dish";
    this.price = options.price || 0;
  }

  description = function () {
    console.log(`${this.constructor.name}: ${this.name}`);
  };
}
