export default class Pasta {
  constructor(options) {
    this.name = options.name;
    this.ingredients = options.ingredients;
    this.price = options.price;
  }

  description = function () {
    console.log(
      `${this.constructor.name} ${this.name}: \n Ingredients: ${this.ingredients}`
    );
  };
}
