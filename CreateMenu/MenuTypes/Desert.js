export default class Desert {
  constructor(options) {
    this.name = options.name || "new desert";
    this.price = options.price || 0;
  }

  description = function () {
    console.log(`${this.constructor.name}: ${this.name}`);
  };
}
