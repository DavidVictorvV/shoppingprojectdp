export default class Drinks {
  constructor(options) {
    this.name = options.name || "new drink";
    this.price = options.price || 0;
  }

  description = function () {
    console.log(`${this.constructor.name}: ${this.name}`);
  };
}
