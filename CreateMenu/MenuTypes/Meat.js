export default class Meat {
  constructor(options) {
    this.name = options.name;
    this.price = options.price;
  }

  description = function () {
    console.log(`${this.constructor.name} ${this.name}`);
  };
}
