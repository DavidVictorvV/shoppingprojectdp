import Pizza from "./MenuTypes/Pizza.js";
import Pasta from "./MenuTypes/Pasta.js";
import Desert from "./MenuTypes/Desert.js";
import Drinks from "./MenuTypes/Drinks.js";
import Meat from "./MenuTypes/Meat.js";
import Sauce from "./MenuTypes/Sauce.js";
import SideDish from "./MenuTypes/SideDish.js";

function PizzaFactory() {
  this.create = function (options) {
    return new Pizza(options);
  };
}

function PastaFactory() {
  this.create = function (options) {
    return new Pasta(options);
  };
}
function DesertFactory() {
  this.create = function (options) {
    return new Desert(options);
  };
}
function DrinksFactory() {
  this.create = function (options) {
    return new Drinks(options);
  };
}
function MeatFactory() {
  this.create = function (options) {
    return new Meat(options);
  };
}
function SauceFactory() {
  this.create = function (options) {
    return new Sauce(options);
  };
}
function SideDishFactory() {
  this.create = function (options) {
    return new SideDish(options);
  };
}

function postItem(item) {
  axios({
    method: "POST",
    url: `https://designpaterns-db6fc-default-rtdb.europe-west1.firebasedatabase.app/Menu/${item.constructor.name}.json`,
    data: item,
  });
}

$("#inputList").load("./CreateMenu/Createinputs/pizza.html");
function showInput() {
  $("#inputList").load(
    `./CreateMenu/Createinputs/${
      document.getElementById("selectItem").value
    }.html`
  );
}

function createItemIgredient() {
  const itemName = document.getElementById("menuItemName").value;
  const itemIngredient = document.getElementById("menuItemIngredients").value;
  const itemPrice = document.getElementById("menuItemPrice").value;

  let selectedType = document.getElementById("selectItem").value;
  let createItem;

  if (selectedType == "pizza") {
    var pizzaFactory = new PizzaFactory();
    createItem = pizzaFactory.create({
      name: itemName,
      ingredients: itemIngredient,
      price:
        parseFloat(Number(itemPrice) * 1.09).toFixed(2) +
        "$" +
        "(VAT included)",
    });
  } else if (selectedType == "pasta") {
    var pastaFactory = new PastaFactory();
    createItem = pastaFactory.create({
      name: itemName,
      rarity: itemIngredient,
      price:
        parseFloat(Number(itemPrice) * 1.09).toFixed(2) +
        "$" +
        "(VAT included)",
    });
  }
  postItem(createItem);
}
function createItemSimple() {
  const itemName = document.getElementById("menuItemName").value;
  const itemPrice = document.getElementById("menuItemPrice").value;

  let selectedType = document.getElementById("selectItem").value;
  let createItem;
  console.log(selectedType);
  if (selectedType == "desert") {
    var desertFactory = new DesertFactory();
    createItem = desertFactory.create({
      name: itemName,
      price:
        parseFloat(Number(itemPrice) * 1.1).toFixed(2) + "$" + "(VAT included)",
    });
  } else if (selectedType == "drinks") {
    var drinksFactory = new DrinksFactory();
    createItem = drinksFactory.create({
      name: itemName,
      price:
        parseFloat(Number(itemPrice) * 1.12).toFixed(2) +
        "$" +
        "(VAT included)",
    });
  } else if (selectedType == "meat") {
    var meatFactory = new MeatFactory();
    createItem = meatFactory.create({
      name: itemName,
      price:
        parseFloat(Number(itemPrice) * 1.09).toFixed(2) +
        "$" +
        "(VAT included)",
    });
  } else if (selectedType == "sauce") {
    var sauceFactory = new SauceFactory();
    createItem = sauceFactory.create({
      name: itemName,
      price: itemPrice + "$" + "(VAT included)",
    });
  } else if (selectedType == "sidedish") {
    var sideDishFactory = new SideDishFactory();
    createItem = sideDishFactory.create({
      name: itemName,
      price: itemPrice,
    });
  }
  postItem(createItem);
}

function createItem() {
  let selectedType = document.getElementById("selectItem").value;
  if (selectedType == "pizza" || selectedType == "pasta") {
    createItemIgredient();
  } else {
    createItemSimple();
  }
  $("#inputList").load(
    `./CreateMenu/Createinputs/${
      document.getElementById("selectItem").value
    }.html`
  );
}

window.showInput = showInput;
window.createItem = createItem;
