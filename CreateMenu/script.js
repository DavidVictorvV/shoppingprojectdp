//factory pattern
import Pizza from "./MenuTypes/Pizza.js";
import Pasta from "./MenuTypes/Pasta.js";

class MenuFactory {
  constructor() {}
  createMenu(options) {
    switch (options.menuType) {
      case "pizza":
        this.menuClass = Pizza;
        break;
      case "pasta":
        this.menuClass = Pasta;
        break;
    }

    return new this.menuClass(options);
  }
}

function createItem() {
  const itemName = document.getElementById("menuItemName").value;
  const itemIngredient = document.getElementById("menuItemIngredients").value;
  const itemPrice = document.getElementById("menuItemCrustType").value;
  if (document.getElementById("selectItem").value == "1")
    createPizza(itemName, itemIngredient, itemPrice);
  else if (document.getElementById("selectItem").value == "2")
    createPasta(itemName, itemIngredient, itemPrice);
}

function PastaFactory() {}
function createPasta() {
  PastaFactory.prototype = new MenuFactory();
  PastaFactory.prototype.menuClass = Pasta;
  MenuFactory.prototype.menuClass = Pasta;
  var pastaFactory = new PastaFactory();

  var newPasta = pastaFactory.createMenu({
    name: itemName,
  });
}

function PizzaFactory() {}
function createPizza(itemName, itemIngredient, itemPrice) {
  PizzaFactory.prototype = new MenuFactory();
  PizzaFactory.prototype.menuClass = Pizza;

  MenuFactory.prototype.menuClass = Pizza;

  var pizzaFactory = new PizzaFactory();

  var newPizza = pizzaFactory.createMenu({
    name: itemName,
    ingredients: itemIngredient,
    price: itemPrice,
  });
  postItem(newPizza);
}
window.createItem = createItem;

function postItem(item) {
  axios({
    method: "POST",
    url: `https://designpaterns-db6fc-default-rtdb.europe-west1.firebasedatabase.app/Menu/${PizzaFactory.prototype.menuClass.name}.json`,
    data: item,
  });
}
// axios({
//   method: "GET",
//   url: "https://designpaterns-db6fc-default-rtdb.europe-west1.firebasedatabase.app/.json",
//   dataType: "jsonp",
//   jsonp: "getbible",
// }).then(function (res) {
//   console.log(res.data);
// });
