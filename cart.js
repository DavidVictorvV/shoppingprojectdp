function createCard({ id, name, ingredients, price, ...item }, type) {
  let card = document.createElement("div");
  card.className = "card mt-4";
  card.style = "width: 18rem";

  let cardBody = document.createElement("div");
  cardBody.className = "card-body d-flex flex-column";

  let title = document.createElement("h5");
  title.className = "card-title";
  title.innerHTML = name + " (" + type + ")";

  let text = document.createElement("div");
  text.className = "card-text";
  if (ingredients) text.innerHTML = ingredients;

  if (type == "Meat") {
    let meatType = document.createElement("select");
    meatType.className = "form-select";
    var rare = document.createElement("option");
    rare.innerHTML = "Rare";
    var medium = document.createElement("option");
    medium.innerHTML = "Medium";
    var well = document.createElement("option");
    well.innerHTML = "Well cooked";
    meatType.appendChild(rare);
    meatType.appendChild(medium);
    meatType.appendChild(well);
    text.appendChild(meatType);
  }

  let priceEl = document.createElement("div");
  priceEl.className = "float-end";
  priceEl.innerHTML = price + "RON" + "(VAT included)";

  let buttonDiv = document.createElement("div");

  let link = document.createElement("a");
  link.className = "btn btn-primary float-end";
  let cart = window.localStorage.getItem("cart");
  link.innerHTML = "Add to cart";
  if (cart.includes(id)) link.innerHTML = "Remove";
  link.onclick = () => {
    removeItem(id);
    card.remove();
  };

  cardBody.appendChild(title);
  cardBody.appendChild(text);
  text.appendChild(priceEl);

  cardBody.appendChild(buttonDiv);
  buttonDiv.appendChild(link);
  card.appendChild(cardBody);
  cardContainer = document.getElementById("cardContainer");
  cardContainer.appendChild(card);
}

function removeItem(id) {
  let cart = JSON.parse(window.localStorage.getItem("cart"));
  let newList = cart;
  for (let i = 0; i < newList.length; i++) {
    if (newList[i].id == id) {
      newList.splice(newList[i], 1);
      newList.splice(newList[i - 1], 1);
    }
  }

  let storage = window.localStorage;
  storage.setItem("cart", JSON.stringify(newList));
  if (cart.length == 0) emptyText();
  else updateTotalPrice();
}
function emptyText() {
  let empty = document.createElement("div");
  empty.className = "row text-center";
  empty.style = "padding-top: 20%";

  let text = document.createElement("h1");
  text.innerHTML = "Your shopping cart is empty...";

  empty.appendChild(text);
  cardContainer = document.getElementById("cardContainer");
  cardContainer.appendChild(empty);
}

function updateTotalPrice() {
  let cart = JSON.parse(window.localStorage.getItem("cart"));
  let totalPrice = 0;
  for (let i = 0; i < cart.length; i++) {
    if (typeof cart[i] == "object") {
      totalPrice += Number(cart[i].price);
    }
  }
  document.getElementById("totalPrice").innerHTML =
    totalPrice + "$ (VAT included)";
}

function run() {
  let cart = JSON.parse(window.localStorage.getItem("cart"));
  let totalPrice = 0;
  if (cart.length > 0) {
    for (let i = 0; i < cart.length; i++) {
      if (typeof cart[i] == "object") {
        totalPrice += Number(cart[i].price);
        createCard(cart[i], cart[i - 1]);
      }
    }
    document.getElementById("totalPrice").innerHTML =
      totalPrice + "$ (VAT included)";
  } else {
    emptyText();
  }
}

run();
