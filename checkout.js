function checkout() {
  let cart = JSON.parse(window.localStorage.getItem("cart"));
  let totalPrice = 0;
  if (cart.length > 0) {
    for (let i = 0; i < cart.length; i++) {
      if (typeof cart[i] == "object") {
        console.log(cart[i].name + ": " + cart[i].price + " RON");
        totalPrice += Number(cart[i].price);
      }
    }
    console.log("----------------------------------");
    console.log("Total:" + totalPrice + " RON");
  } else {
    console.log("Empty cart");
  }
}
